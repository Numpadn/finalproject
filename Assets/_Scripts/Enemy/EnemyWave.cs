﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWave : MonoBehaviour
{
    //Have a reference to the enemyWaveManager

    //Bool to check if player killed all enemies
    //public bool isKilledEnemies = false;

    //Have a list for the enemies inside the prefab

    void Start()
    {
        //Add all the enemy children to the new list
    }

    void Update()
    {
        //Check how many enemies are left in the wave 
        CheckEnemyCount();
    }

    void CheckEnemyCount()
    {
        //If there are no more enemies left in the list, set isKilledEnemies to true and 
        //start the timer for the next wave (Which is on the enemy wave manager)
    }
}
