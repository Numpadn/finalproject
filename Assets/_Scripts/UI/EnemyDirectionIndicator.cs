﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDirectionIndicator : MonoBehaviour
{
    //This is the arrow UI which is instantiated for each enemy
    public GameObject arrowPrefab;

    //We need to reference the enemyManager so we have access to it
    public EnemyManager enemyManager;

    //This is a list for all of the arrows which are added into the game
    public List<RectTransform> arrows = new List<RectTransform>();

    //This is a reference to the players position which is needed to find..
    //.. the relevance of the player from the enemy
    public Transform player;

    //This whole function moves the arrows around to where we need them
    void MoveArrows()
    {
        //This sets the angle for each arrow to point in for each enemy
        for(int i = 0; i < enemyManager.allEnemies.Count; i++)
        {
            //This gets the transform for each seperate enemy
            Transform enemy = enemyManager.allEnemies[i].transform;

            //This compares the enemy position with the players position and..
            //..then normalizes it which makes the Vector have a magnitude..
            //..of 1, but keeps the direction.
            Vector3 enemyDirection = enemy.position - player.position;
            enemyDirection.Normalize();

            //This line was just for debug purposes to check what worked
            Debug.DrawLine(player.position, player.position + enemyDirection, Color.red);

            //This gets the float in angles between the players forward position..
            //..and the enemyDirection (Which was set above)
            float angle = Vector3.Angle(player.forward, enemyDirection);

            //Since Vector3.Angle can never be greater than 180, the arrows..
            //.. would always only go in one direction if an enemy flies..
            //..behind you, which isn't helpful if you're in the other..
            //..direction, so this makes it so the angle is between..
            //..180 and -180, so it basically is 360 degrees.
            if(player.forward.y < enemyDirection.y)
            {
                angle = -angle;
            }
            //This would set each arrows rotation, but keeping the z and..
            //..x axis as 0 since they don't need to move
            arrows[i].rotation = Quaternion.Euler(0,0,angle);
            arrows[i].gameObject.SetActive(true);
        }

        //this deactivates the arrow if the enemy isn't there
        for(int j = enemyManager.allEnemies.Count; j < arrows.Count; j++)
        {
            arrows[j].gameObject.SetActive(false);
        }
    }

    void SpawnArrows()
    {
        //This adds the arrows which we need for each enemy into the scene
        while(arrows.Count < enemyManager.allEnemies.Count)
        {
            GameObject newArrow = Instantiate(arrowPrefab, transform);
            arrows.Add(newArrow.GetComponent<RectTransform>());
        }
    }
    
    void Update()
    {   
        SpawnArrows();
        MoveArrows();
    }
}
