﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyIcon : MonoBehaviour
{
    //If the enemy is this close to the same height, they're considered level
    public float heightThreshold = 20f;

    //These are each of the sprites which are changed when the enemy is..
    //..above, below, or level with the player on the mini-map
    [Header("Sprites")]
    public Sprite levelSprite;
    public Sprite aboveSprite;
    public Sprite belowSprite;

    //These are the colours which the sprites are changed to when the enemy..
    //..is above, below, or level with the player on the mini-map.
    [Header("Colours")]
    public Color32 levelColor;
    public Color32 aboveColor;
    public Color32 belowColor;

    //The rendering component of this object;
    Image image;

    //The 2D transform component of this object;
    RectTransform rectTransform;

    public void SetPosition(Vector2 position, float maxDistance)
    {
        //clamp the distance if it's too far
        if(position.magnitude > maxDistance)
        {
            position = position.normalized * maxDistance;
        }
        //set the position
        rectTransform.anchoredPosition = position;
    }

    public void SetIcon(Vector3 playerPos, Vector3 enemyPos)
    {
        //get the height difference
        float heightDiff = enemyPos.y - playerPos.y;

        //work out if above or below
        bool isAbove = heightDiff > heightThreshold;
        bool isBelow = heightDiff < -heightThreshold;

        //set the colours and the sprites
        if(isAbove)
        {
            image.sprite = aboveSprite;
            image.color = aboveColor;
        }
        else if(isBelow)
        {
            image.sprite = belowSprite;
            image.color = belowColor;
        }
        else
        {
            image.sprite = levelSprite;
            image.color = levelColor;
        }

    }

    void Awake()
    {
        image = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
    }
}
