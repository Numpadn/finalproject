﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRadar : MonoBehaviour
{
    public GameObject iconPrefab;
    public Transform iconParent;

    public float radarClampDistance = 100f;
    public Transform radarReference;
    public float radarScale;

    [Header("Player components")]
    public Transform playerObj;
    public RectTransform playerIcon;

    List<EnemyIcon> iconList = new List<EnemyIcon>();

    void Update()
    {
        CreateNewIcons();
        ManageIcons();
        SetIconPosition();
        SetPlayerRotation();
    }

    void SetIconPosition()
    {
        //reference the enemylist from the enemymanager
        List<EnemyScript> enemyList = EnemyManager.instance.allEnemies;
        
        //reference the playerpos from the radarRef
        Vector3 playerPos = radarReference.position;

        //for loop for each enemy 
        for(int i = 0; i < enemyList.Count; i++)
        {
            //get the position for the current enemy and then..
            //..work out where the enemy is compared to the player
            Vector3 enemyPos = enemyList[i].transform.position;
            Vector3 differenceVector = radarReference.InverseTransformPoint(enemyPos);
            Vector2 difference2D = new Vector2(differenceVector.x, differenceVector.z);

            //set the position and apperance of the current icon
            iconList[i].SetPosition(difference2D * radarScale, radarClampDistance);
            iconList[i].SetIcon(playerPos, enemyPos);
        }

    }

    void SetPlayerRotation()
    {
        //Get the difference between the players rotation..
        //..and the cameras rotation
        float angle = Vector3.SignedAngle(playerObj.forward, radarReference.forward,Vector3.up);
        //set the rotation of the player icon (rectTransfrom)
        playerIcon.rotation = Quaternion.Euler(0,0,angle);
    }

    void CreateNewIcons()
    {
        //Only create the icons when needed and then we can..
        //..hide them when they're not needed.
        while(iconList.Count < EnemyManager.instance.allEnemies.Count)
        {
            GameObject newObj = Instantiate(iconPrefab, iconParent);
            iconList.Add(newObj.GetComponent<EnemyIcon>());
        }
    }

    void ManageIcons()
    {
        //Only have icons for however many enemies we have in..
        //..the scene and hide the rest
        int enemyCount = EnemyManager.instance.allEnemies.Count;

        for(int i = 0; i < iconList.Count; i++)
        {
            bool shouldBeActive = i < enemyCount;
            iconList[i].gameObject.SetActive(shouldBeActive);
        }
    }
}
