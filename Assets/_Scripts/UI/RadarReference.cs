﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarReference : MonoBehaviour
{
    public Transform playerObject;
    public Transform cameraParent;

    void Update()
    {
        //This makes it so the objects position is the player's but..
        //.. it's rotation is the cameras, we need this to work out..
        //..where the enemies are relative to the player.
        transform.position = playerObject.position;
        transform.rotation = cameraParent.rotation;      
    }
}
