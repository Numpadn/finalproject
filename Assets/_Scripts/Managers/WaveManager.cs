﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class WaveData
{
    public GameObject wavePrefab;
    public float waitTime;
    public float currentTime;
    public Transform spawner;
    public float intensityIncrease;
}


public class WaveManager : MonoBehaviour
{
    //List of data for each wave
    public List<WaveData> allWaves = new List<WaveData>();
    public CutsceneCounter cutsceneCounter;
    public PlayerController playerController;

    void Update()
    {
        //if there's no more enemies left in enemy wave, start wave delay
        if(EnemyManager.EnemyCount() > 0)
        {
            return;
        }
        if(allWaves.Count == 0)
        {
            //cutsceneCounter.Count = 2;
            cutsceneCounter.Count++;
            SceneManager.LoadScene("Cutscene");
            return;
        }
        WaveData currentWave = allWaves[0];
        currentWave.currentTime += Time.deltaTime;
        if(currentWave.currentTime >= currentWave.waitTime)
        {
            Instantiate(currentWave.wavePrefab, currentWave.spawner.position, currentWave.spawner.rotation);
            allWaves.RemoveAt(0);
            playerController.intensity = currentWave.intensityIncrease;
        }
    }
}
