﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{

    public float timeBeforeCutsceneMovie = 50f;
    public bool cutsceneIsPlay = false;
    public CutsceneCounter cutsceneCounter;
    public EventSystem eventSys;
    public GameObject buttonSubMenu;

    [Header("UI Groups")]
    public GameObject mainMenu;
    public GameObject subMenu;
    public GameObject loadGame;
    public GameObject missionSelectScreen;
    public GameObject options;

    public void OnClickNewGame()
    {
        cutsceneCounter.Count = 1;
        SceneManager.LoadScene("Cutscene");
    }

    public void OnClickLoadGame()
    {
        subMenu.SetActive(false);
        loadGame.SetActive(true);
    }

    public void OnClickLoadGameFile()
    {
        loadGame.SetActive(false);
        missionSelectScreen.SetActive(true);
    }

    public void OnClickOptions()
    {
        subMenu.SetActive(false);
        options.SetActive(true);
    }

    public void OnClickBack()
    {
        options.SetActive(false);
        loadGame.SetActive(false);
        missionSelectScreen.SetActive(false);
        subMenu.SetActive(true);
    }

    void Update()
    {
        timeBeforeCutsceneMovie -= Time.deltaTime;

        if (timeBeforeCutsceneMovie <= 0 && cutsceneIsPlay == false)
        {
            cutsceneIsPlay = true;
            cutsceneCounter.Count = 0;
            SceneManager.LoadScene("Cutscene");
        }

        if (Input.GetButtonDown("Square"))
        {
            mainMenu.SetActive(false);
            subMenu.SetActive(true);
            eventSys.SetSelectedGameObject(buttonSubMenu);
        }
    }
}
