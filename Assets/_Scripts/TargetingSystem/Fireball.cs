﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [Header("Enemy script reference")]
    public EnemyScript enemy;

    [Header("Fireball speed")]
    public float moveSpeed;

    [Header("Damage")]
    public int damageDealt;

    void Update()
    {
        //This checks if there is an enemy to fire at
        if (enemy != null)
        {
            MoveTowardsEnemy();
        }
    }

    void MoveTowardsEnemy()
    {
        //This checks the distance between the player and the enemy
        Vector3 vectorToTarget = enemy.transform.position - transform.position;

        if (vectorToTarget.magnitude < 0.1f) //This checks if the target has hit the enemy.
        {
            enemy.TakeDamage(damageDealt);
            if(enemy.enemyHealth <= 0)
            {
                Destroy(gameObject);
            }
            Destroy(gameObject);
        }

        //this moves the fireball towards the enemy.
        transform.position += vectorToTarget.normalized * Time.deltaTime * moveSpeed;
    }
}
