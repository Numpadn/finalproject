﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetIndicator : MonoBehaviour
{
    [Header("Targeting properties")]
    public GameObject targetPrefab;
    public TargetingSystem targetingSystem;
    public List<RectTransform> rects;

    Canvas canvas;
    Camera cam;

    //This function has a counter on the side of what target..
    //..you are on, since there is a limit on targets.
    void SpawnIcons()
    {
        while (rects.Count < targetingSystem.targetedEnemies.Count)
        {
            GameObject newOb = Instantiate(targetPrefab, transform);
            newOb.GetComponentInChildren<Text>().text = (rects.Count + 1).ToString();
            rects.Add(newOb.GetComponent<RectTransform>());
        }
    }

    //This function moves the crosshairs to the position which the..
    //..enemies are in through GUI and not in a 3D space.
    void MoveIconsToEnemies()
    {
        for (int i = 0; i < targetingSystem.targetedEnemies.Count; i++)
        {
            Vector3 worldPos = targetingSystem.targetedEnemies[i].transform.position;
            Vector3 screenPos = cam.WorldToScreenPoint(worldPos);
            Vector2 canvasPos = screenPos / canvas.scaleFactor;
            if (screenPos.z < 0)
            {
                canvasPos = new Vector2(-500, -500);
            }
            rects[i].anchoredPosition = canvasPos;
        }
        for (int j = targetingSystem.targetedEnemies.Count; j < rects.Count; j++)
        {
            rects[j].anchoredPosition = new Vector2(-500, -500);
        }
    }

    void Start()
    {
        cam = Camera.main;
        canvas = GetComponent<Canvas>();
    }

    void Update()
    {
        SpawnIcons();
        MoveIconsToEnemies();
    }
}
