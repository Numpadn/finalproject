﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Cutscene Counter", menuName = "ScriptableObjects/CutsceneCounter")]
public class CutsceneCounter : ScriptableObject
{
    [SerializeField] private int cutsceneCount = 0;
    public int Count = 0;
}