﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class CutsceneDetectDone : MonoBehaviour
{
    public bool isDonePlaying = false;
    public CutsceneManager cutsceneManager;

    void Start()
    {
        bool isDonePlaying = false;
    }

    public void IsDonePlayingCutscene()
    {
        if (GetComponent<PlayableDirector>().state != PlayState.Playing && isDonePlaying == false)
        {
            isDonePlaying = true;
        }
    }

    void Update()
    {
        IsDonePlayingCutscene();
    }
}
