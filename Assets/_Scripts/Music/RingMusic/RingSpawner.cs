﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingSpawner : MonoBehaviour
{
    public timingData timeData;
    public TextAsset textAsset;
    public int blackIndex;
    public int whiteIndex;
    public AudioSource audioSource;
    public AudioSource bellSound;

    public GameObject blackRing;
    public GameObject whiteRing;

    void Start()
    {
        timeData = JsonUtility.FromJson<timingData>(textAsset.text);
    }


    void Update()
    {
        if(blackIndex < timeData.blackTime.Count)
        {
            float nextTime = timeData.blackTime[blackIndex];

            if(audioSource.time > nextTime)
            {
                var blackIns = Instantiate(blackRing);
                blackIns.transform.parent = transform;
                blackIns.transform.position = transform.position;
                blackIndex ++;
            }
        }
        if(whiteIndex < timeData.whiteTime.Count)
        {
            float nextTime = timeData.whiteTime[whiteIndex];

            if(audioSource.time > nextTime)
            {
                var whiteIns = Instantiate(whiteRing);
                whiteIns.transform.parent = transform;
                whiteIns.transform.position = transform.position;
                whiteIndex ++;
            }
        }
        
    }
}
