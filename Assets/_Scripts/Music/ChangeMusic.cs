﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusic : MonoBehaviour
{
    [Header("FMOD")]
    public FMODUnity.StudioEventEmitter emitter;
    public FMODUnity.StudioEventEmitter emitter_health;

    public PlayerController playerController;

    public float level1Max;
    public float level2Max;
    public float level3Max;

    public int levelNumber;


    public void Update()
    {
        if (levelNumber == 1)
        {
            emitter.SetParameter("Intensity_L1", playerController.intensity * level1Max);
        }
        if (levelNumber == 2)
        {
            emitter.SetParameter("Intensity_L2", playerController.intensity * level2Max);
        }
        if (levelNumber == 3)
        {
            emitter.SetParameter("Intensity_L3", playerController.intensity * level3Max);
        }
        
        emitter.SetParameter("Health", playerController.health);  
        emitter_health.SetParameter("Health", playerController.health);    
    }

    public void Start()
    {
        
    }

}
