﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController123 : MonoBehaviour
{
    public float speed;
    public float moveSpeed;
    public float rotateSpeed;
    public float upwardsSpeed = 0.03f;
    public float rotationZ;

    [Header("Timer properties")]
    public float coolDown_SpeedUpTime = 2f;
    public bool coolDown_SpeedUp = false;
    public AudioSource soundFX;
    public AudioClip comeOn;
    public float coolDownFromSpeed = 10f;
    public bool coolDownFromSpeedBool = false;

    [Header("Parameters")]
    [Range(0, 1)]
    public float intensity = 0.2f;
    [Range(0, 100)]
    public int health = 100;

    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
        speed -= transform.forward.y * Time.deltaTime * 50.0f;

        if (speed < 10.0f && coolDown_SpeedUp == false)
        {
            speed = 10.0f;
        }
        if (speed > 60.0f && coolDown_SpeedUp == false)
        {
            speed = 60.0f;
        }
        transform.Rotate(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), -0.01f);

        //This checks if the cooldown isn't activated and if the player is holding R2 on a 
        //PS3 controller, it also checks if the player has recently used this boost.
        if (coolDown_SpeedUp == false && Input.GetButton("R2") && coolDownFromSpeedBool == false)
        {
            //This if statement controls the players speed and plays the taunt sound
            //It also sets the bool values.
            speed = 100f;
            soundFX.PlayOneShot(comeOn);
            coolDown_SpeedUp = true;
            coolDownFromSpeedBool = true;
        }
        //This checks if the cooldown has been activated.
        if (coolDown_SpeedUp == true)
        {
            //This counts down the countDown every second.
            coolDown_SpeedUpTime -= Time.deltaTime;

            //This checks if boost has ran out 
            if (coolDown_SpeedUpTime <= 0)
            {
                //This sets the default speed and then sets the bool for the cooldown being active
                //this also sets back the speed up time so it doesn't go into the negatives
                speed = 30f;
                coolDown_SpeedUp = false;
                coolDown_SpeedUpTime = 2f;
            }
        }
        //this checks if there has been a sensible amount of time between boosts, this if statement
        //runs if the player has boosted or not
        if (coolDownFromSpeedBool == true)
        {
            //This then counts down the timer between boosts
            coolDownFromSpeed -= Time.deltaTime;

            //This checks if the cooldown has reached 0
            if (coolDownFromSpeed <= 0)
            {
                //This sets the cooldown bool and then resets it.
                coolDownFromSpeedBool = false;
                coolDownFromSpeed = 10f;
            }
        }
    }
}