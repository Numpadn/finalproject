﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;
using UnityEngine.Rendering.PostProcessing;

public class Test : MonoBehaviour
{
    private Transform playerModel;
    public Transform playerParent;

    [Header("Flying Parameters")]
    public float xySpeed = 18;
    public float lookSpeed = 340;
    public float forwardSpeed = 6;
    public float speed;

    [Space]

    [Header("Public References")]
    public Transform aimTarget;
    public CinemachineDollyCart dolly;
    public Transform cameraParent;

    [Space]

    [Header("Particles")]
    public ParticleSystem trail;
    public ParticleSystem circle;
    public ParticleSystem barrel;
    public ParticleSystem stars;

    [Space]

    [Header("Timer properties")]
    public float coolDown_SpeedUpTime = 2f;
    public bool coolDown_SpeedUp = false;
    public AudioSource soundFX;
    public AudioClip comeOn;
    public float coolDownFromSpeed = 10f;
    public bool coolDownFromSpeedBool = false;

    [Space]

    [Header("Music Parameters")]
    [Range(0, 1)]
    public float intensity = 0.2f;
    [Range(0, 100)]
    public int health = 100;

    void Start()
    {
        playerModel = transform.GetChild(0);
        
    }


    void Update()
    {
        float hori = Input.GetAxis("Horizontal");
        float verti = Input.GetAxis("Vertical");

        LocalMove(hori, verti, xySpeed);
        RotationLook(hori, verti, lookSpeed);
        HorizontalLean(playerModel, hori, 80, 0.1f);

        if (Input.GetButtonDown("R2") || Input.GetButtonDown("L2"))
        {   
            int dir = Input.GetButtonDown("L2") ? -1 : 1;
            QuickSpin(dir);  
        }     
        
        transform.position += transform.forward * Time.deltaTime * speed;
        speed -= transform.forward.y * Time.deltaTime * 50.0f;

        if (speed < 10.0f && coolDown_SpeedUp == false)
        {
            speed = 10.0f;
        }
        if (speed > 60.0f && coolDown_SpeedUp == false)
        {
            speed = 60.0f;
        }
        transform.Rotate(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), -0.01f);

        //This checks if the cooldown isn't activated and if the player is holding R2 on a 
        //PS3 controller, it also checks if the player has recently used this boost.
        if (coolDown_SpeedUp == false && Input.GetButton("R2") && coolDownFromSpeedBool == false)
        {
            //This if statement controls the players speed and plays the taunt sound
            //It also sets the bool values.
            speed = 100f;
            soundFX.PlayOneShot(comeOn);
            coolDown_SpeedUp = true;
            coolDownFromSpeedBool = true;
        }
        //This checks if the cooldown has been activated.
        if (coolDown_SpeedUp == true)
        {
            //This counts down the countDown every second.
            coolDown_SpeedUpTime -= Time.deltaTime;

            //This checks if boost has ran out 
            if (coolDown_SpeedUpTime <= 0)
            {
                //This sets the default speed and then sets the bool for the cooldown being active
                //this also sets back the speed up time so it doesn't go into the negatives
                speed = 30f;
                coolDown_SpeedUp = false;
                coolDown_SpeedUpTime = 2f;
            }
        }
        //this checks if there has been a sensible amount of time between boosts, this if statement
        //runs if the player has boosted or not
        if (coolDownFromSpeedBool == true)
        {
            //This then counts down the timer between boosts
            coolDownFromSpeed -= Time.deltaTime;

            //This checks if the cooldown has reached 0
            if (coolDownFromSpeed <= 0)
            {
                //This sets the cooldown bool and then resets it.
                coolDownFromSpeedBool = false;
                coolDownFromSpeed = 10f;
            }
        }
    }

    void LocalMove(float x, float y, float speed)
    {
        transform.localPosition += new Vector3(x, y, 0) * speed * Time.deltaTime;
        ClampPosition();
    }

    void ClampPosition()
    {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }

    void RotationLook(float h, float v, float speed)
    {
        aimTarget.parent.position = Vector3.zero;
        aimTarget.localPosition = new Vector3(h, v, 1);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(aimTarget.position), Mathf.Deg2Rad * speed * Time.deltaTime);

    }

    void HorizontalLean(Transform target, float axis, float leanLimit, float lerpTime)
    {
        Vector3 targetEulerAngles = target.localEulerAngles;
        target.localEulerAngles = new Vector3(targetEulerAngles.x, targetEulerAngles.y, Mathf.LerpAngle(targetEulerAngles.z, -axis * leanLimit, lerpTime));
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(aimTarget.position, 0.5f);
        Gizmos.DrawSphere(aimTarget.position, 0.15f);
    }

    //This is a public void which takes an direction input when it is called
    public void QuickSpin(int dir)
    {
        //This if statement checks if the player model is already tweening, and if it isn't it runs
        if(!DOTween.IsTweening(playerParent))
        {
            //This line of code rotates the player model in this case, in the x axis by what direction it is in.
            //DOLocalRotate takes a target rotation and animated toward it, so we take our current rotation and rotate it by 360 multiplied by direction
            //(DOLocalRotate(Vector3 to, float duration, RotateMode mode) Rotates the target's localRotation to the given value.)
            //LocalEulerAngles is a represenataion of the player rotation relative to it's parent
            //Outside of the vector3, we have 0.4f which is the duration and rotatemode is the way it interprets the movement, LocalAxisAdd Adds the given rotation to the transform's local axis
            //And SetEase is a way of smoothing out a transition
            playerParent.DOLocalRotate(new Vector3(360 * -dir, playerParent.localEulerAngles.y, playerParent.localEulerAngles.z), 0.4f, RotateMode.LocalAxisAdd).SetEase(Ease.OutSine);
            barrel.Play();
        }
    }

    void SetSpeed(float x)
    {
        dolly.m_Speed = x;
    }

    void SetCameraZoom(float zoom, float duration)
    {
        cameraParent.DOLocalMove(new Vector3(0, 0, zoom), duration);
    }

    void DistortionAmount(float x)
    {
        Camera.main.GetComponent<PostProcessVolume>().profile.GetSetting<LensDistortion>().intensity.value = x;
    }

    void FieldOfView(float fov)
    {
        cameraParent.GetComponentInChildren<CinemachineVirtualCamera>().m_Lens.FieldOfView = fov;
    }

    void Chromatic(float x)
    {
        Camera.main.GetComponent<PostProcessVolume>().profile.GetSetting<ChromaticAberration>().intensity.value = x;
    }

}