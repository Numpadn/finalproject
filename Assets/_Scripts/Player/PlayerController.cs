﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    [Header("Managers")]
    public CrosshairManager crosshairManager;

    [Header("Flying Properties")]
    public float speed;
    public float moveSpeed;
    public float rotateSpeed;
    public float upwardsSpeed = 0.03f;
    public float rotationZ;
    public float flyingSensitivity = 2;

    [Header("Animation Properties")]
    public float flapSpeed;
    public Transform RotTransform;
    public Animator anim;
    public Transform dragonTransform;

    [Header("Dodge Properties")]
    public float currentDodgeSpeed;
    public float maxDodgeSpeed = 150;
    public float glideThreshold = 30f;

    [Header("Particle Properties")]
    public ParticleSystem barrelRoll;

    [Header("Timer Properties")]
    public float coolDown_SpeedUpTime = 2f;
    public bool isCurrentlyBoosting = false;
    public float coolDownFromSpeed = 2f;
    public bool coolDownFromSpeedBool = false;

    [Header("Audio Properties")]
    public AudioSource soundFX;
    public AudioSource voiceLine;
    public AudioSource playerHit;
    public AudioClip hurtSound;
    public AudioClip glideSound;
    public AudioClip growlSound;

    [Header("Parameters")]
    [Range(0, 1)]
    public float intensity = 0.2f;
    [Range(0, 100)]
    public int health = 100;

    [Header("PostProcessing")]
    public Camera cam;
    public PostProcessVolume postProcessVolume;
    private ColorGrading colorGrading;

    public void Start()
    {
        postProcessVolume.profile.TryGetSettings(out colorGrading);
        health = 100;
    }

    public void TakeDamage(int damageDealt)
    {
        playerHit.PlayOneShot(hurtSound);
        Debug.Log("Player hit by enemy");
        //Postprocess
        health = health - damageDealt;
    }
    void ControlAnims()
    {
        bool isDodging = DOTween.IsTweening(RotTransform);
        bool isFlapping = speed < glideThreshold;
        if(isCurrentlyBoosting)
        {
            isFlapping = true;
        }
        anim.SetFloat("FlapSpeed", flapSpeed);
        anim.SetBool("Flap", isFlapping);
        anim.SetBool("IsDodging", isDodging);
    }

    void Update()
    {
        colorGrading.saturation.value = health - 70f;
        ControlAnims();
        transform.position += transform.forward * Time.deltaTime * speed;
        HandleDodge();
        speed -= transform.forward.y * Time.deltaTime * 50.0f;
        //postProcessing which would update with health

        if(health <= 0)
        {
            Debug.Log("Dead");
            health = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetButtonDown("R1") || Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.X) || Input.GetButtonDown("L1") || Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Z))
        {   
            int dir = Input.GetButtonDown("L1") || Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Z) ? -1 : 1;
            QuickSpin(dir);  
        }

        if (Input.GetButtonDown("R2") || Input.GetKeyDown(KeyCode.R) || Input.GetMouseButtonDown(1))
        {   
            QuickFullSpin();  
        }

        if (speed < 10.0f && isCurrentlyBoosting == false)
        {
            speed = 10.0f;
        }
        if (speed > 60.0f && isCurrentlyBoosting == false)
        {
            speed = 60.0f;
        }
        if(speed > 45f)
        {
            //Postprocessing
            //sound fx
        }
        
        Vector2 rotateAmount = crosshairManager.GetRotateAmount();
        transform.Rotate(rotateAmount.x / flyingSensitivity, rotateAmount.y / flyingSensitivity, 0);
        if(Input.GetButtonDown("L2") || Input.GetKeyDown(KeyCode.Space))
        {
            if (isCurrentlyBoosting == false && coolDownFromSpeedBool == false)
            {
                speed = 100f;
                flapSpeed = 3f;
                voiceLine.PlayOneShot(glideSound);
                soundFX.PlayOneShot(growlSound);
                isCurrentlyBoosting = true;
                coolDownFromSpeedBool = true;
                //postProcessing
            }
            return;
        }


        if (isCurrentlyBoosting == true)
        {
            coolDown_SpeedUpTime -= Time.deltaTime;

            if (coolDown_SpeedUpTime <= 0)
            {
                speed = 30f;
                flapSpeed = 1f;
                isCurrentlyBoosting = false;
                coolDown_SpeedUpTime = 2f;
            }
        }
        if (coolDownFromSpeedBool == true)
        {
            coolDownFromSpeed -= Time.deltaTime;
            if (coolDownFromSpeed <= 0)
            {
                coolDownFromSpeedBool = false;
                coolDownFromSpeed = 2f;
            }
        }
    }
    public void QuickSpin(int dir)
    {
        if(!DOTween.IsTweening(RotTransform))
        {
            RotTransform.DOLocalRotate(new Vector3(0, 0, 360 * -dir), 0.4f, RotateMode.LocalAxisAdd).SetEase(Ease.OutSine);
            currentDodgeSpeed = dir * maxDodgeSpeed;
            barrelRoll.Play();
        }
    }

    public void QuickFullSpin()
    {
        if(!DOTween.IsTweening(dragonTransform))
        {
            dragonTransform.DOLocalRotate(new Vector3(0, 180, 0), 0.4f, RotateMode.LocalAxisAdd).SetEase(Ease.OutSine);
            barrelRoll.Play();
        }
    }

    void HandleDodge()
    {
        transform.position += transform.right * currentDodgeSpeed * Time.deltaTime;
        currentDodgeSpeed = Mathf.Lerp(currentDodgeSpeed, 0, Time.deltaTime * 10f);
    }
}
