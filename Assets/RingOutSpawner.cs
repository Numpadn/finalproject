﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingOutSpawner : MonoBehaviour
{
    public GameObject blackRing_Out;
    public GameObject whiteRing_Out;

    public float delay;

    void SpawnNewRing(GameObject toSpawn)
    {
        var newObj = Instantiate(toSpawn);
        newObj.transform.parent = transform; 
        newObj.transform.position = transform.position;
        RingOut ring = newObj.GetComponent<RingOut>();
        ring.spawner = this;
    }

    void Update()
    {
        if(delay <= 0)
        {
            if(Input.GetButtonDown("Square") || Input.GetKeyDown(KeyCode.Z))
            {
                SpawnNewRing(whiteRing_Out);
            }
            if(Input.GetButtonDown("Triangle")|| Input.GetKeyDown(KeyCode.X))
            {
                SpawnNewRing(blackRing_Out);
            }
        }
        else
        {
            delay -= Time.deltaTime;
        }

    }
}
