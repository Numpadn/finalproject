﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuScroll : MonoBehaviour
{
    Transform controls;

    void Start()
    {
        controls = GetComponent<Transform>();
    }
    
    public void LeftButton()
    {
        controls.localPosition = new Vector3(250f, 0f, 0f);
    }
    
    public void RightButton()
    {
        controls.localPosition = new Vector3(-250f, 0f, 0f);
    }

    public void MiddleButton()
    {
        controls.localPosition = new Vector3(0f, 0f, 0f);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
